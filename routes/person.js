const { response } = require('express')
const express = require('express')
const { request } = require('http')
const db = require('../db')
const utils = require('../utils')
const cryptoJs = require('crypto-js')

const router = express.Router()

router.get('/',(request,response)=>{
    const query= `select id, firstName, email, password from person`
    db.pool.execute(query,(error,person)=>{
    response.send(utils.createresult(error, person))
    })
})

router.post('/',(request,response)=>{
    const { firstName, lastName, email, password } = request.body

    console.log(firstName+" " + lastName+ " " + email+" "+password)
    const encryptedPassword = String(cryptoJs.MD5(password))
    const query = `insert into person (firstName, lastName, email, password) values(?,?,?,?)`
    db.pool.execute(query,[firstName,lastName,email,password],
        (error, result)=>{
            response.send(utils.createresult(error, result))
        })
})

module.exports = router