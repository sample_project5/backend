const mysql = require('mysql2')

const pool = mysql.createPool({
    host: '172.17.0.3',
    user: 'root',
    password: 'manager',
    connectionLimit: 10,
    database: 'my_db'
})

module.exports = {
    pool,
}
